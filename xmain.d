/*
 * Copyright (c) 2006-2007 Erin Catto http://box2d.org
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies.
 * Erin Catto makes no representations about the suitability
 * of this software for any purpose.
 * It is provided "as is" without express or implied warranty.
 *
 * Changes by Ketmar // Invisible Vector
 */
module xmain;

import arsd.simpledisplay;

import iv.glbinds.utils;
import iv.vmath;

import b2dlite;

bool optShowTimes = false;
bool optDrawBVH = false;


// ////////////////////////////////////////////////////////////////////////// //
enum GWidth = 800;
enum GHeight = 600;


// ////////////////////////////////////////////////////////////////////////// //
__gshared BodyContact[] contacts;


// ////////////////////////////////////////////////////////////////////////// //
// random number in range [-1,1]
public VFloat rnd () {
  pragma(inline, true);
  import std.random : uniform;
  return cast(VFloat)uniform!"[]"(-VFloatNum!1, VFloatNum!1);
}

public VFloat rnd (VFloat lo, VFloat hi) {
  pragma(inline, true);
  import std.random : uniform;
  return cast(VFloat)uniform!"[]"(lo, hi);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared BodyBase bomb = null;

__gshared VFloat timeStep = VFloatNum!1/VFloatNum!60;
__gshared int iterations = 10;
__gshared Vec2 gravity = Vec2(VFloatNum!0, -VFloatNum!10);

__gshared int demoIndex = 0;
__gshared string demoTitle;

__gshared bool onlyOneBomb = false;
__gshared int launchesLeft = 0;

__gshared World world;
shared static this () { world = new World(gravity, iterations); }


// ////////////////////////////////////////////////////////////////////////// //
void launchBomb (SimpleWindow sdwin) {
  if (!onlyOneBomb || bomb is null) {
    Vec2[] vtx;
    foreach (int ang; 0..10) {
      import std.math : cos, sin;
      import std.random : uniform;
      Vec2 v = Vec2(0.6*cos(deg2rad(360/10*ang))*uniform!"[]"(0.7, 1.3), 0.6*sin(deg2rad(360/10*ang))*uniform!"[]"(0.7, 1.3));
      vtx ~= v;
    }
    bomb = new PolyBody(world, vtx, 50);
    //bomb = PolyBody.Box(Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!50);
    bomb.friction = VFloatNum!(0.2);
  }
  bomb.setPosition(rnd(-VFloatNum!15, VFloatNum!15), VFloatNum!15);
  bomb.rotation = rnd(-VFloatNum!(1.5), VFloatNum!(1.5));
  bomb.velocity = bomb.position*(-VFloatNum!(1.5));
  bomb.angularVelocity = rnd(-VFloatNum!20, VFloatNum!20);
  import std.format : format;
  sdwin.title = "%s -- bodies: %s".format(demoTitle, world.bodies.length);
}


// ////////////////////////////////////////////////////////////////////////// //
void drawBody (BodyBase bodyb) {
  if (auto booody = cast(PolyBody)bodyb) {
    if (booody is bomb) {
      glColor3f(VFloatNum!(0.4), VFloatNum!(0.9), VFloatNum!(0.4));
    } else {
      glColor3f(VFloatNum!(0.8), VFloatNum!(0.8), VFloatNum!(0.9));
    }
    auto rmt = booody.rmat;
    glBegin(GL_LINE_LOOP);
    foreach (const ref vx; booody.verts) {
      auto vp = booody.position+rmt*vx;
      glVertex2f(vp.x, vp.y);
    }
    glEnd();
  }
}


void drawJoint (Joint joint) {
  auto b1 = joint.body0;
  auto b2 = joint.body1;

  auto r1 = b1.rmat;
  auto r2 = b2.rmat;

  Vec2 x1 = b1.position;
  Vec2 p1 = x1+r1*joint.localAnchor1;

  Vec2 x2 = b2.position;
  Vec2 p2 = x2+r2*joint.localAnchor2;

  glColor3f(VFloatNum!(0.5), VFloatNum!(0.5), VFloatNum!(0.8));
  glBegin(GL_LINES);
  glVertex2f(x1.x, x1.y);
  glVertex2f(p1.x, p1.y);
  glVertex2f(x2.x, x2.y);
  glVertex2f(p2.x, p2.y);
  glEnd();
}


void drawWorld () {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(VFloatNum!0, -VFloatNum!7, -VFloatNum!25);

  // draw world
  foreach (BodyBase b; world.bodies) b.drawBody();
  foreach (Joint j; world.joints) j.drawJoint();

  // draw contacts
  {
    import iv.glbinds;
    glPointSize(VFloatNum!4);
    glColor3f(VFloatNum!1, VFloatNum!0, VFloatNum!0);
    glBegin(GL_POINTS);
    foreach (const ref cxy; contacts) glVertex2f(cxy.position.x, cxy.position.y);
    glEnd();
    glPointSize(VFloatNum!1);
  }

  if (optDrawBVH) {
    // draw BVH
    world.drawBVH((Vec2 min, Vec2 max) {
      glColor3f(VFloatNum!1, VFloatNum!1, VFloatNum!0);
      glBegin(GL_LINE_LOOP);
      glVertex2f(min.x, min.y);
      glVertex2f(max.x, min.y);
      glVertex2f(max.x, max.y);
      glVertex2f(min.x, max.y);
      glEnd();
    });
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// demos
enum BoxW = VFloatNum!100;
enum BoxH = VFloatNum!20;

struct DemoInfo { string dsc; }


// single box
@DemoInfo("A Single Box") void demo1 () {
  PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
  });
  PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!200, (b, size) {
    b.setPosition(VFloatNum!0, VFloatNum!4);
  });
}


// a simple pendulum
@DemoInfo("Simple Pendulum") void demo2 () {
  auto b1 = PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
    b.rotation = VFloatNum!0;
  });

  auto b2 = PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!100, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!9, VFloatNum!11);
    b.rotation = VFloatNum!0;
  });

  new Joint(world, b1, b2, Vec2(VFloatNum!0, VFloatNum!11));
}


// varying friction coefficients
@DemoInfo("Varying Friction Coefficients") void demo3 () {
  PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
  });

  PolyBody.Box(world, Vec2(VFloatNum!13, VFloatNum!(0.25)), VFloat.max, (b, size) {
    b.setPosition(-VFloatNum!2, VFloatNum!11);
    b.rotation = -VFloatNum!(0.25);
  });

  PolyBody.Box(world, Vec2(VFloatNum!(0.25), VFloatNum!1), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!(5.25), VFloatNum!(9.5));
  });

  PolyBody.Box(world, Vec2(VFloatNum!13, VFloatNum!(0.25)), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!2, VFloatNum!7);
    b.rotation = VFloatNum!(0.25);
  });

  PolyBody.Box(world, Vec2(VFloatNum!(0.25), VFloatNum!1), VFloat.max, (b, size) {
    b.setPosition(-VFloatNum!(5.25), VFloatNum!(5.5));
  });

  PolyBody.Box(world, Vec2(VFloatNum!13, VFloatNum!(0.25)), VFloat.max, (b, size) {
    b.setPosition(-VFloatNum!2, VFloatNum!3);
    b.rotation = -VFloatNum!(0.25);
  });

  static immutable VFloat[5] frictions = [
    VFloatNum!(0.75),
    VFloatNum!(0.5),
    VFloatNum!(0.35),
    VFloatNum!(0.1),
    VFloatNum!0
  ];
  foreach (immutable idx, VFloat frc; frictions) {
    PolyBody.Box(world, Vec2(VFloatNum!(0.5), VFloatNum!(0.5)), VFloatNum!25, (b, size) {
      b.friction = frc;
      b.setPosition(-VFloatNum!(7.5)+VFloatNum!2*cast(int)idx, VFloatNum!14);
    });
  }
}


// a vertical stack
@DemoInfo("Randomized Stacking") void demo4 () {
  PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
    b.rotation = VFloatNum!0;
  });

  foreach (int idx; 0..10) {
    PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!1, (b, size) {
      b.friction = VFloatNum!(0.2);
      VFloat x = rnd(-VFloatNum!(0.1), VFloatNum!(0.1));
      b.setPosition(x, VFloatNum!(0.51)+VFloatNum!(1.05)*idx);
    });
  }
}


// a pyramid
@DemoInfo("Pyramid Stacking") void demo5 () {
  PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
    b.rotation = VFloatNum!0;
  });

  Vec2 x = Vec2(-VFloatNum!6, VFloatNum!(0.75));
  foreach (int idx; 0..12) {
    auto y = x;
    foreach (int j; idx..12) {
      PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!10, (b, size) {
        b.friction = VFloatNum!(0.2);
        b.position = y;
      });
      y += Vec2(VFloatNum!(1.125), VFloatNum!0);
    }
    //x += Vec2(VFloatNum!(0.5625), VFloatNum!(1.125));
    x += Vec2(VFloatNum!(0.5625), VFloatNum!2);
  }
}


// a teeter
@DemoInfo("A Teeter") void demo6 () {
  auto b1 = PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
  });

  auto b2 = PolyBody.Box(world, Vec2(VFloatNum!12, VFloatNum!(0.25)), VFloatNum!100, (b, size) {
    b.setPosition(VFloatNum!0, VFloatNum!1);
  });

  new Joint(world, b1, b2, Vec2(VFloatNum!0, VFloatNum!1));

  PolyBody.Box(world, Vec2(VFloatNum!(0.5), VFloatNum!(0.5)), VFloatNum!25, (b, size) {
    b.setPosition(-VFloatNum!5, VFloatNum!2);
  });

  PolyBody.Box(world, Vec2(VFloatNum!(0.5), VFloatNum!(0.5)), VFloatNum!25, (b, size) {
    b.setPosition(-VFloatNum!(5.5), VFloatNum!2);
  });

  PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!1), VFloatNum!100, (b, size) {
    b.setPosition(VFloatNum!(5.5), VFloatNum!15);
  });
}


// a suspension bridge
@DemoInfo("A Suspension Bridge") void demo7 () {
  import std.math : PI;

  PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
    b.rotation = VFloatNum!0;
  });

  enum numPlanks = 15;
  VFloat mass = VFloatNum!50;
  foreach (int idx; 0..numPlanks) {
    PolyBody.Box(world, Vec2(VFloatNum!1, VFloatNum!(0.25)), mass, (b, size) {
      b.friction = VFloatNum!(0.2);
      b.setPosition(-VFloatNum!(8.5)+VFloatNum!(1.25)*idx, VFloatNum!5);
    });
  }

  // tuning
  VFloat frequencyHz = VFloatNum!2;
  VFloat dampingRatio = VFloatNum!(0.7);
  // frequency in radians
  VFloat omega = VFloatNum!2*PI*frequencyHz;
  // damping coefficient
  VFloat d = VFloatNum!2*mass*dampingRatio*omega;
  // spring stifness
  VFloat k = mass*omega*omega;
  // magic formulas
  VFloat softnesss = VFloatNum!1/(d+timeStep*k);
  VFloat biasFactorr = timeStep*k/(d+timeStep*k);

  foreach (int idx; 0..numPlanks) {
    new Joint(world, world.bodies[idx], world.bodies[idx+1], Vec2(-VFloatNum!(9.125)+VFloatNum!(1.25)*idx, VFloatNum!5), (j) {
      j.softness = softnesss;
      j.biasFactor = biasFactorr;
    });
  }

  new Joint(world, world.bodies[numPlanks], world.bodies[0], Vec2(-VFloatNum!(9.125)+VFloatNum!(1.25)*numPlanks, VFloatNum!5), (j) {
    j.softness = softnesss;
    j.biasFactor = biasFactorr;
  });
}


// dominos
@DemoInfo("Dominos") void demo8 () {
  auto b1 = PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
  });

  PolyBody.Box(world, Vec2(VFloatNum!12, VFloatNum!(0.5)), VFloat.max, (b, size) {
    b.setPosition(-VFloatNum!(1.5), VFloatNum!10);
  });

  foreach (int idx; 0..10) {
    PolyBody.Box(world, Vec2(VFloatNum!(0.2), VFloatNum!2), VFloatNum!100, (b, size) {
      b.setPosition(-VFloatNum!6+VFloatNum!1*idx, VFloatNum!(11.125));
      b.friction = VFloatNum!(0.1);
    });
  }

  PolyBody.Box(world, Vec2(VFloatNum!14, VFloatNum!(0.5)), VFloat.max, (b, size) {
    b.setPosition(VFloatNum!1, VFloatNum!6);
    b.rotation = VFloatNum!(0.3);
  });

  auto b2 = PolyBody.Box(world, Vec2(VFloatNum!(0.5), VFloatNum!3), VFloat.max, (b, size) {
    b.setPosition(-VFloatNum!7, VFloatNum!4);
  });

  auto b3 = PolyBody.Box(world, Vec2(VFloatNum!12, VFloatNum!(0.25)), VFloatNum!20, (b, size) {
    b.setPosition(-VFloatNum!(0.9), VFloatNum!1);
  });

  new Joint(world, b1, b3, Vec2(-VFloatNum!2, VFloatNum!1));

  auto b4 = PolyBody.Box(world, Vec2(VFloatNum!(0.5), VFloatNum!(0.5)), VFloatNum!10, (b, size) {
    b.setPosition(-VFloatNum!10, VFloatNum!15);
  });

  new Joint(world, b2, b4, Vec2(-VFloatNum!7, VFloatNum!15));

  auto b5 = PolyBody.Box(world, Vec2(VFloatNum!2, VFloatNum!2), VFloatNum!12, (b, size) {
    b.setPosition(VFloatNum!6, VFloatNum!(2.5));
    b.friction = VFloatNum!(0.1);
  });

  new Joint(world, b1, b5, Vec2(VFloatNum!6, VFloatNum!(2.6)));

  // box cap
  auto b6 = PolyBody.Box(world, Vec2(VFloatNum!2, VFloatNum!(0.2)), VFloatNum!10, (b, size) {
    b.setPosition(VFloatNum!6, VFloatNum!(3.6));
  });

  new Joint(world, b5, b6, Vec2(VFloatNum!7, VFloatNum!(3.5)));
}


// a multi-pendulum
@DemoInfo("Multi-pendulum") void demo9 () {
  import std.math : PI;

  auto b1 = PolyBody.Box(world, Vec2(BoxW, BoxH), VFloat.max, (b, size) {
    b.friction = VFloatNum!(0.2);
    b.setPosition(VFloatNum!0, -VFloatNum!(0.5)*size.y);
    b.rotation = VFloatNum!0;
  });

  enum VFloat mass = VFloatNum!10;

  // tuning
  enum VFloat frequencyHz = VFloatNum!4;
  enum VFloat dampingRatio = VFloatNum!(0.7);
  // frequency in radians
  enum VFloat omega = VFloatNum!2*PI*frequencyHz;
  // damping coefficient
  enum VFloat d = VFloatNum!2*mass*dampingRatio*omega;
  // spring stiffness
  enum VFloat k = mass*omega*omega;
  // magic formulas
  VFloat softnesss = VFloatNum!1/(d+timeStep*k);
  VFloat biasFactorr = timeStep*k/(d+timeStep*k);

  enum VFloat y = VFloatNum!12;
  foreach (int idx; 0..15) {
    auto x = Vec2(VFloatNum!(0.5)+idx, y);
    auto b = PolyBody.Box(world, Vec2(VFloatNum!(0.75), VFloatNum!(0.25)), mass, (b, size) {
      b.friction = VFloatNum!(0.2);
      b.position = x;
      b.rotation = VFloatNum!0;
    });

    new Joint(world, b1, b, Vec2(idx, y), (j) {
      j.softness = softnesss;
      j.biasFactor = biasFactorr;
    });

    b1 = b;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool paused = false;
__gshared bool doOneStep = false;
__gshared bool slowmo = false;
__gshared int slowmocount = 0;


bool setupDemo (int index, SimpleWindow w) {
  import std.traits;
  alias sms = getSymbolsByUDA!(mixin(__MODULE__), DemoInfo);
  foreach (immutable idx, const memb; sms) {
    if (index == idx) {
      demoTitle = getUDAs!(memb, DemoInfo)[0].dsc;
      w.title = demoTitle;
      world.clear();
      bomb = null;
      memb();
      demoIndex = index;
      return true;
    }
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
void simulationStep (SimpleWindow sdwin) {
  if (launchesLeft > 0) {
    --launchesLeft;
    auto oob = onlyOneBomb;
    onlyOneBomb = false;
    scope(exit) onlyOneBomb = oob;
    launchBomb(sdwin);
  }

  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(VFloatNum!0, -VFloatNum!7, -VFloatNum!25);

  contacts.length = 0;
  contacts.assumeSafeAppend;

  import core.time;
  MonoTime stt;
  if (optShowTimes) stt = MonoTime.currTime;
  world.step(timeStep);
  if (optShowTimes) {
    auto tm = (MonoTime.currTime-stt).total!"msecs";
    { import core.stdc.stdio; printf("step: %d msecs\n", cast(int)tm); }
  }
  sdwin.redrawOpenGlSceneNow();
}


// ////////////////////////////////////////////////////////////////////////// //
void showOpts () {
  import std.stdio;
  writeln("simulation options:");
  writeln("  accumutate impulses: ", World.accumulateImpulses);
  writeln("  position correction: ", World.positionCorrection);
  writeln("  warm starting      : ", World.warmStarting);
}


// ////////////////////////////////////////////////////////////////////////// //
void main () {
  //setOpenGLContextVersion(3, 2); // up to GLSL 150
  //openGLContextCompatible = false;

  b2dlDrawContactsCB = delegate (in ref BodyContact ctx) {
    contacts ~= ctx;
  };

  auto sdwin = new SimpleWindow(GWidth, GHeight, "Box2DLite Physics", OpenGlOptions.yes, Resizablity.fixedSize);
  //sdwin.hideCursor();

  sdwin.redrawOpenGlScene = delegate () {
    glClear(GL_COLOR_BUFFER_BIT);
    drawWorld();
  };

  sdwin.visibleForTheFirstTime = delegate () {
    showOpts();
    setupDemo(0, sdwin);
    sdwin.setAsCurrentOpenGlContext(); // make this window active
    glViewport(0, 0, GWidth, GHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    oglPerspective(45.0, cast(VFloat)GWidth/cast(VFloat)GHeight, 0.1, 100.0);
    sdwin.redrawOpenGlScene();
  };

  sdwin.eventLoop(1000/60,
    delegate () {
      if (sdwin.closed || world is null) return;
      if (!paused) {
        doOneStep = false;
        if (slowmo) {
          if (--slowmocount < 0) {
            simulationStep(sdwin);
            slowmocount = 10;
          }
        } else {
          simulationStep(sdwin);
          slowmocount = 0;
        }
      } else if (doOneStep) {
        doOneStep = false;
        simulationStep(sdwin);
      }
    },
    delegate (KeyEvent event) {
      if (sdwin.closed) return;
      if (!event.pressed) return;
      switch (event.key) {
        case Key.Escape: sdwin.close(); break;
        default:
      }
    },
    delegate (MouseEvent event) {
    },
    delegate (dchar ch) {
      if (ch == 'q') { sdwin.close(); return; }
      if (ch == 'r') { setupDemo(demoIndex, sdwin); return; }
      if (ch == ' ') { paused = !paused; return; }
      if (ch == 's') { slowmo = !slowmo; return; }
      if (ch >= '1' && ch <= '9') { setupDemo(ch-'1', sdwin); return; }
      if (ch == '0') { setupDemo(10, sdwin); return; }
      if (ch == 'a') { World.accumulateImpulses = !World.accumulateImpulses; showOpts(); return; }
      if (ch == 'p') { World.positionCorrection = !World.positionCorrection; showOpts(); return; }
      if (ch == 'w') { World.warmStarting = !World.warmStarting; showOpts(); return; }
      if (ch == '\n' || ch == '\r') { launchBomb(sdwin); return; }
      if (ch == '+') { setupDemo(demoIndex+1, sdwin); return; }
      if (ch == '-') { if (demoIndex > 0) setupDemo(demoIndex-1, sdwin); return; }
      if (ch == 'z') { doOneStep = true; return; }
      if (ch == 'T') { optShowTimes = !optShowTimes; return; }
      if (ch == 'A') { optDrawBVH = !optDrawBVH; return; }
      if (ch == 'O') { onlyOneBomb = !onlyOneBomb; return; }
      if (ch == '!') { launchesLeft = 1100; return; }
    },
  );
}
