/*
 * Copyright (c) 2006-2007 Erin Catto http://box2d.org
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies.
 * Erin Catto makes no representations about the suitability
 * of this software for any purpose.
 * It is provided "as is" without express or implied warranty.
 *
 * Changes by Ketmar // Invisible Vector
 * basically, i replaced boxes with convex polygon bodies, and added
 * universal SAT solver, based on Randy Gaul code
 */
module b2dlite;
private:

public import iv.vmath;

version = b2dlite_use_bvh;
version = b2dlite_bvh_many_asserts;


// ////////////////////////////////////////////////////////////////////////// //
public alias Vec2 = VecN!(2, float);
public alias VFloat = Vec2.VFloat;
public alias VFloatNum = Vec2.VFloatNum;


// ////////////////////////////////////////////////////////////////////////// //
public struct BodyContact {
  Vec2 position;
  Vec2 normal;
  VFloat separation;
}

public void delegate (in ref BodyContact ctx) b2dlDrawContactsCB;


// ////////////////////////////////////////////////////////////////////////// //
public struct Mat22 {
nothrow @safe @nogc:
public:
  Vec2 col1, col2;

public:
  this (VFloat angle) { pragma(inline, true); set(angle); }

  void set (VFloat angle) {
    pragma(inline, true);
    import core.stdc.math : cosf, sinf;
    immutable VFloat c = cosf(angle), s = sinf(angle);
    col1.x =  c; col1.y = s;
    col2.x = -s; col2.y = c;
  }

pure:
  this() (in auto ref Vec2 acol1, in auto ref Vec2 acol2) { pragma(inline, true); col1 = acol1; col2 = acol2; }

  Mat22 transpose () const { pragma(inline, true); return Mat22(Vec2(col1.x, col2.x), Vec2(col1.y, col2.y)); }

  Mat22 invert () const {
    pragma(inline, true);
    immutable VFloat a = col1.x, b = col2.x, c = col1.y, d = col2.y;
    Mat22 bm;
    VFloat det = a*d-b*c;
    assert(det != VFloatNum!0);
    det = VFloatNum!1/det;
    bm.col1.x = det*d;
    bm.col2.x = -det*b;
    bm.col1.y = -det*c;
    bm.col2.y = det*a;
    return bm;
  }

  Vec2 opBinary(string op:"*") (in auto ref Vec2 v) const { pragma(inline, true); return Vec2(col1.x*v.x+col2.x*v.y, col1.y*v.x+col2.y*v.y); }

  Mat22 opBinary(string op:"+") (in auto ref Mat22 bm) const { pragma(inline, true); return Mat22(col1+bm.col1, col2+bm.col2); }
  Mat22 opBinary(string op:"*") (in auto ref Mat22 bm) const { pragma(inline, true); return Mat22(this*bm.col1, this*bm.col2); }

  Mat22 abs() () { pragma(inline, true); return Mat22(col1.abs, col2.abs); }
}


// ////////////////////////////////////////////////////////////////////////// //
private Vec2 fcross() (VFloat s, in auto ref Vec2 a) { pragma(inline, true); return Vec2(-s*a.y, s*a.x); }


// ////////////////////////////////////////////////////////////////////////// //
// world
public final class World {
private:
  static struct ArbiterKey {
    BodyBase body0, body1;

    this (BodyBase b0, BodyBase b1) { if (b0 < b1) { body0 = b0; body1 = b1; } else { body0 = b1; body1 = b0; } }

    bool opEquals() (in auto ref ArbiterKey b) const {
      pragma(inline, true);
      return (body0 == b.body0 && body1 == b.body1);
    }

    int opCmp() (in auto ref ArbiterKey b) const {
      if (body0 is null) {
        if (b.body0 !is null) return -1;
        if (body1 is null) return (b.body1 !is null ? -1 : 0);
        return body1.opCmp(b.body1);
      }
      if (int r = body0.opCmp(b.body0)) return r;
      if (body1 is null) return (b.body1 !is null ? -1 : 0);
      return body1.opCmp(b.body1);
    }

    size_t toHash () {
      return hashu32((body0 !is null ? body0.midx : 0))^hashu32((body1 !is null ? body1.midx : 0));
    }

  static:
    uint hashu32 (uint a) pure nothrow @safe @nogc {
      a -= (a<<6);
      a ^= (a>>17);
      a -= (a<<9);
      a ^= (a<<4);
      a -= (a<<3);
      a ^= (a<<10);
      a ^= (a>>15);
      return a;
    }
  }

private:
  version(b2dlite_use_bvh) {
    DynamicAABBTree bvh;
  }

  // start from frame 1, to automatically make all new objects invalid even if no step was done
  uint frameNum = 1;

public:
  BodyBase[] bodies;
  Joint[] joints;
  Arbiter[ArbiterKey] arbiters;
  Vec2 gravity;
  int iterations;

  // the following are world options
  static bool accumulateImpulses = true;
  static bool warmStarting = true;
  static bool positionCorrection = true;

public:
  this() (in auto ref Vec2 agravity, int aiterations) {
    gravity = agravity;
    iterations = aiterations;
    version(b2dlite_use_bvh) bvh = new DynamicAABBTree(VFloatNum!0.2); // gap
  }

  void add (BodyBase bbody) {
    if (bbody !is null) {
      if (bbody.mWorld is this) return; // nothing to do, it is already here
      if (bbody.mWorld !is null) throw new Exception("body cannot be owned by two worlds");
      bbody.mWorld = this;
      bodies ~= bbody;
      version(b2dlite_use_bvh) bbody.nodeId = bvh.addObject(bbody);
    }
  }

  void add (Joint joint) {
    if (joint !is null) {
      if (joint.mWorld is this) return; // nothing to do, it is already here
      if (joint.mWorld !is null) throw new Exception("joint cannot be owned by two worlds");
      joint.mWorld = this;
      joints ~= joint;
    }
  }

  void opOpAssign(string op:"~", T) (T v) if (is(T : BodyBase) || is(T : Joint)) { add(v); }

  void clear () {
    bodies = null;
    joints = null;
    arbiters.clear();
    version(b2dlite_use_bvh) bvh.reset();
  }

  void step (VFloat dt) {
    if (dt <= VFloatNum!0) return;
    ++frameNum; // new frame (used to track arbiters)
    VFloat invDt = (dt > VFloatNum!0 ? VFloatNum!1/dt : VFloatNum!0);
    // determine overlapping bodies and update contact points
    broadPhase();
    // integrate forces
    foreach (BodyBase b; bodies) {
      if (b.invMass == VFloatNum!0) continue;
      b.velocity += (gravity+b.force*b.invMass)*dt;
      b.angularVelocity += dt*b.invI*b.torque;
    }
    // perform pre-steps
    foreach (ref Arbiter arb; arbiters.byValue) {
      if (arb.frameNum == frameNum && arb.active) arb.preStep(invDt);
    }
    foreach (Joint j; joints) j.preStep(invDt);
    // perform iterations
    foreach (immutable itnum; 0..iterations) {
      foreach (ref Arbiter arb; arbiters.byValue) {
        if (arb.frameNum == frameNum && arb.active) arb.applyImpulse();
      }
      foreach (Joint j; joints) j.applyImpulse();
    }
    // integrate velocities
    foreach (BodyBase b; bodies) {
      auto disp = b.velocity*dt;
      b.mPosition += b.velocity*dt;
      b.mRotation += b.angularVelocity*dt;
      b.force.set(VFloatNum!0, VFloatNum!0);
      b.torque = VFloatNum!0;
      version(b2dlite_use_bvh) bvh.updateObject(b.nodeId, disp);
    }
  }

  void broadPhase () {
    version(b2dlite_use_bvh) {
      foreach (immutable idx, BodyBase bi; bodies) {
        auto aabb = bi.getAABB();
        bvh.reportAllShapesOverlappingWithAABB(aabb, (int nodeId) {
          auto bj = bvh.getNodeBody(nodeId);
          if (bj == bi) return;
          if (bi.invMass == VFloatNum!0 && bj.invMass == VFloatNum!0) return;
          auto ak = ArbiterKey(bi, bj);
          if (auto arb = ak in arbiters) {
            if (arb.frameNum != frameNum) arb.setup(bi, bj, frameNum);
          } else {
            arbiters[ak] = Arbiter(bi, bj, frameNum);
          }
        });
      }
    } else {
      // O(n^2) broad-phase
      foreach (immutable idx, BodyBase bi; bodies) {
        foreach (BodyBase bj; bodies[idx+1..$]) {
          if (bi.invMass == VFloatNum!0 && bj.invMass == VFloatNum!0) continue;
          if (auto arb = ArbiterKey(bi, bj) in arbiters) {
            arb.setup(bi, bj, frameNum);
          } else {
            arbiters[ArbiterKey(bi, bj)] = Arbiter(bi, bj, frameNum);
          }
        }
      }
    }
  }

  void drawBVH (scope void delegate (Vec2 min, Vec2 max) dg) {
    version(b2dlite_use_bvh) {
      bvh.forEachLeaf((int nodeId, in ref AABB aabb) {
        dg(aabb.mMin, aabb.mMax);
      });
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// body
public abstract class BodyBase {
private:
  static shared uint lastidx;

private:
  uint midx;
  Mat22 mRmattmp; // undefined most of the time; used only in coldet for now
  uint rmatFrameNum; // when rmattmp was cached last time?
  AABB mAABB; // cached AABB
  uint aabbFrameNum; // when mAABB was cached last time?
  int nodeId; // in bvh
  World mWorld;

public:
  Vec2 mPosition;
  VFloat mRotation;

  Vec2 velocity;
  VFloat angularVelocity;

  Vec2 force;
  VFloat torque;

  VFloat friction;
  VFloat mass, invMass;
  VFloat i, invI;

private:
  void updateWorld () {
    version(b2dlite_use_bvh) {
      if (mWorld !is null) {
        mWorld.bvh.updateObject(nodeId, Vec2(VFloatNum!0, VFloatNum!0), true); // force reinsert
      }
    }
  }

public:
  this () @trusted {
    import core.atomic : atomicOp;
    midx = atomicOp!"+="(lastidx, 1);

    mPosition.set(VFloatNum!0, VFloatNum!0);
    mRotation = VFloatNum!0;
    velocity.set(VFloatNum!0, VFloatNum!0);
    angularVelocity = VFloatNum!0;
    force.set(VFloatNum!0, VFloatNum!0);
    torque = VFloatNum!0;
    friction = VFloatNum!(0.2);
    mass = VFloat.max;
    invMass = VFloatNum!0;
    i = VFloat.max;
    invI = VFloatNum!0;
  }

  @property uint id () const pure nothrow @safe @nogc { pragma(inline, true); return midx; }

  void addForce() (in auto ref Vec2 f) pure nothrow @safe @nogc { pragma(inline, true); force += f; }

  final ref const(Vec2) position () const pure nothrow @safe @nogc { pragma(inline, true); return mPosition; }
  final void position() (in auto ref Vec2 p) { pragma(inline, true); mPosition = p; updateWorld(); }
  final void setPosition() (VFloat ax, VFloat ay) { pragma(inline, true); mPosition.set(ax, ay); updateWorld(); }

  final VFloat rotation () const pure nothrow @safe @nogc { pragma(inline, true); return mRotation; }
  final void rotation() (VFloat aangle) { pragma(inline, true); mRotation = aangle; updateWorld(); }

  override bool opEquals (Object b) const pure nothrow @trusted @nogc {
    //pragma(inline, true);
    if (b is null) return false;
    if (b is this) return true;
    if (auto bb = cast(BodyBase)b) return (bb.midx == midx);
    return false;
  }

  override int opCmp (Object b) const pure nothrow @trusted @nogc {
    //pragma(inline, true);
    if (b is null) return 1;
    if (b is this) return 0;
    if (auto bb = cast(BodyBase)b) return (midx < bb.midx ? -1 : midx > bb.midx ? 1 : 0);
    return -1;
  }

  override size_t toHash () nothrow @safe @nogc {
    return hashu32(midx);
  }

  // get (and cache) AABB
  final ref const(AABB) getAABB () {
    pragma(inline, true);
    if (mWorld is null || aabbFrameNum != mWorld.frameNum) {
      // cache rotation matrix
      mAABB = recalcAABB();
      if (mWorld !is null) aabbFrameNum = mWorld.frameNum;
    }
    return mAABB;
  }

  // called to calculate new AABB for frame
  abstract AABB recalcAABB ();

  // get (and cache) rotation matrix
  public final ref const(Mat22) rmat () nothrow @safe @nogc {
    pragma(inline, true);
    if (mWorld is null || rmatFrameNum != mWorld.frameNum) {
      // cache rotation matrix
      mRmattmp.set(mRotation);
      if (mWorld !is null) rmatFrameNum = mWorld.frameNum;
    }
    return mRmattmp;
  }

static:
  uint hashu32 (uint a) pure nothrow @safe @nogc {
    a -= (a<<6);
    a ^= (a>>17);
    a -= (a<<9);
    a ^= (a<<4);
    a -= (a<<3);
    a ^= (a<<10);
    a ^= (a>>15);
    return a;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class PolyBody : BodyBase {
public:
  Vec2[] verts; // vertices
  Vec2[] norms; // normals

protected:
  this () @trusted {
    super();
    computeMass(VFloatNum!1);
  }

public:
  this (World w, const(Vec2)[] averts, VFloat adensity, scope void delegate (typeof(this) self) dg=null) {
    super();
    set(averts, adensity);
    if (dg !is null) dg(this);
    if (w !is null) w ~= this;
  }

  static PolyBody Box() (World w, in auto ref Vec2 size, VFloat density, scope void delegate (typeof(this) self, Vec2 size) dg=null) {
    auto res = new PolyBody();
    res.setBox(size);
    res.computeMass(density);
    if (dg !is null) dg(res, size);
    if (w !is null) w ~= res;
    return res;
  }

  void set() (const(Vec2)[] averts, VFloat adensity) {
    mPosition.set(VFloatNum!0, VFloatNum!0);
    mRotation = VFloatNum!0;
    velocity.set(VFloatNum!0, VFloatNum!0);
    angularVelocity = VFloatNum!0;
    force.set(VFloatNum!0, VFloatNum!0);
    torque = VFloatNum!0;
    friction = VFloatNum!(0.2);
    setVerts(averts);
    computeMass(adensity);
    updateWorld();
  }

  void computeMass(bool densityIsMass=false) (VFloat density) {
    // calculate centroid and moment of interia
    auto c = Vec2(0, 0); // centroid
    VFloat area = 0;
    VFloat I = 0;
    enum k_inv3 = VFloatNum!1/VFloatNum!3;

    foreach (immutable i1; 0..verts.length) {
      // triangle vertices, third vertex implied as (0, 0)
      auto p1 = verts[i1];
      auto i2 = (i1+1)%verts.length;
      auto p2 = verts[i2];

      VFloat D = p1.cross(p2);
      VFloat triangleArea = VFloatNum!(0.5)*D;

      area += triangleArea;

      // use area to weight the centroid average, not just vertex mPosition
      c += triangleArea*k_inv3*(p1+p2);

      VFloat intx2 = p1.x*p1.x+p2.x*p1.x+p2.x*p2.x;
      VFloat inty2 = p1.y*p1.y+p2.y*p1.y+p2.y*p2.y;
      I += (VFloatNum!(0.25)*k_inv3*D)*(intx2+inty2);
    }

    c *= VFloatNum!1/area;

    // translate vertices to centroid (make the centroid (0, 0) for the polygon in model space)
    // not really necessary, but I like doing this anyway
    foreach (ref v; verts) v -= c;

    if (area > 0 && density < VFloat.max) {
      mass = density*area;
      invMass = VFloatNum!1/mass;
      //i = mass*(size.x*size.x+size.y*size.y)/VFloatNum!12;
      i = I*density;
      invI = VFloatNum!1/i;
    } else {
      mass = VFloat.max;
      invMass = VFloatNum!0;
      i = VFloat.max;
      invI = VFloatNum!0;
    }
  }

  // width and height
  private void setBox (Vec2 size) {
    size /= VFloatNum!2;
    verts.length = 0;
    verts ~= Vec2(-size.x, -size.y);
    verts ~= Vec2( size.x, -size.y);
    verts ~= Vec2( size.x,  size.y);
    verts ~= Vec2(-size.x,  size.y);
    norms.length = 0;
    norms ~= Vec2(VFloatNum!( 0), VFloatNum!(-1));
    norms ~= Vec2(VFloatNum!( 1), VFloatNum!( 0));
    norms ~= Vec2(VFloatNum!( 0), VFloatNum!( 1));
    norms ~= Vec2(VFloatNum!(-1), VFloatNum!( 0));
  }

  private void setVerts (const(Vec2)[] averts) {
    // no hulls with less than 3 vertices (ensure actual polygon)
    if (averts.length < 3) throw new Exception("degenerate body");

    // find the right most point on the hull
    int rightMost = 0;
    VFloat highestXCoord = averts[0].x;
    foreach (immutable i; 1..averts.length) {
      VFloat x = averts[i].x;
      if (x > highestXCoord) {
        highestXCoord = x;
        rightMost = cast(int)i;
      } else if (x == highestXCoord) {
        // if matching x then take farthest negative y
        if (averts[i].y < averts[rightMost].y) rightMost = cast(int)i;
      }
    }

    auto hull = new int[](averts.length);
    int outCount = 0;
    int indexHull = rightMost;
    int vcount = 0;

    for (;;) {
      hull[outCount] = indexHull;

      // search for next index that wraps around the hull by computing cross products to
      // find the most counter-clockwise vertex in the set, given the previos hull index
      int nextHullIndex = 0;
      foreach (immutable i; 1..averts.length) {
        // skip if same coordinate as we need three unique points in the set to perform a cross product
        if (nextHullIndex == indexHull) {
          nextHullIndex = i;
          continue;
        }
        // cross every set of three unique vertices
        // record each counter clockwise third vertex and add to the output hull
        // See : http://www.oocities.org/pcgpe/math2d.html
        auto e1 = averts[nextHullIndex]-averts[hull[outCount]];
        auto e2 = averts[i]-averts[hull[outCount]];
        auto c = e1.cross(e2);
        if (c < 0.0f) nextHullIndex = i;

        // cross product is zero then e vectors are on same line
        // therefore want to record vertex farthest along that line
        if (c == 0.0f && e2.lengthSquared > e1.lengthSquared) nextHullIndex = i;
      }

      ++outCount;
      indexHull = nextHullIndex;

      // conclude algorithm upon wrap-around
      if (nextHullIndex == rightMost) {
        vcount = outCount;
        break;
      }
    }
    if (vcount < 3) throw new Exception("degenerate body");

    // copy vertices into shape's vertices
    foreach (immutable i; 0..vcount) verts ~= averts[hull[i]];
    if (!isConvex()) throw new Exception("non-convex body");

    // compute face normals
    norms.reserve(verts.length);
    foreach (immutable i1; 0..verts.length) {
      immutable i2 = (i1+1)%verts.length;
      auto face = verts[i2]-verts[i1];
      // ensure no zero-length edges, because that's bad
      assert(face.lengthSquared > FLTEPS*FLTEPS);
      // calculate normal with 2D cross product between vector and scalar
      norms ~= Vec2(face.y, -face.x).normalized;
    }
  }

  // the extreme point along a direction within a polygon
  Vec2 support() (in auto ref Vec2 dir) {
    VFloat bestProjection = -VFloat.max;
    Vec2 bestVertex;
    foreach (immutable i; 0..verts.length) {
      Vec2 v = verts[i];
      VFloat projection = v.dot(dir);
      if (projection > bestProjection) {
        bestVertex = v;
        bestProjection = projection;
      }
    }
    return bestVertex;
  }

  bool isConvex () {
    static int sign() (VFloat v) { pragma(inline, true); return (v < 0 ? -1 : v > 0 ? 1 : 0); }
    if (verts.length < 3) return false;
    if (verts.length == 3) return true; // nothing to check here
    int dir;
    foreach (immutable idx, const ref v; verts) {
      auto v1 = Vec2(verts[(idx+1)%verts.length])-v;
      auto v2 = Vec2(verts[(idx+2)%verts.length]);
      int d = sign(v2.x*v1.y-v2.y*v1.x+v1.x*v.y-v1.y*v.x);
      if (d == 0) return false;
      if (idx) {
        if (dir != d) return false;
      } else {
        dir = d;
      }
    }
    return true;
  }

  // return AABB for properly moved and rotated body
  override AABB recalcAABB () {
    AABB res;
    res.mMin = Vec2(float.max, float.max);
    res.mMax = Vec2(-float.max, -float.max);
    auto rmt = Mat22(mRotation);
    foreach (const ref vx; verts) {
      import std.algorithm : max, min;
      auto vp = mPosition+rmt*vx;
      res.mMin.x = min(res.mMin.x, vp.x);
      res.mMin.y = min(res.mMin.y, vp.y);
      res.mMax.x = max(res.mMax.x, vp.x);
      res.mMax.y = max(res.mMax.y, vp.y);
    }
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// joint
public final class Joint {
private:
  World mWorld;

public:
  VFloat biasFactor = VFloatNum!(0.2);
  VFloat softness = VFloatNum!0;

public:
  Mat22 mt;
  Vec2 localAnchor1, localAnchor2;
  Vec2 r1, r2;
  Vec2 bias;
  Vec2 accimp = Vec2(0, 0); // accumulated impulse
  BodyBase body0, body1;

public:
  this() (World w, BodyBase b0, BodyBase b1, in auto ref Vec2 anchor, scope void delegate (typeof(this) self) dg=null) {
    set(b0, b1, anchor);
    if (dg !is null) dg(this);
    if (w !is null) w ~= this;
  }

  void set() (BodyBase b0, BodyBase b1, in auto ref Vec2 anchor) {
    body0 = b0;
    body1 = b1;

    auto rot1 = Mat22(body0.mRotation);
    auto rot2 = Mat22(body1.mRotation);
    Mat22 rot1T = rot1.transpose();
    Mat22 rot2T = rot2.transpose();

    localAnchor1 = rot1T*(anchor-body0.mPosition);
    localAnchor2 = rot2T*(anchor-body1.mPosition);

    accimp.set(VFloatNum!0, VFloatNum!0);

    softness = VFloatNum!0;
    biasFactor = VFloatNum!(0.2);
  }

  void preStep (VFloat invDt) {
    // pre-compute anchors, mass matrix, and bias
    auto rot1 = Mat22(body0.mRotation);
    auto rot2 = Mat22(body1.mRotation);

    r1 = rot1*localAnchor1;
    r2 = rot2*localAnchor2;

    // deltaV = deltaV0+kmt*impulse
    // invM = [(1/m1+1/m2)*eye(2)-skew(r1)*invI1*skew(r1)-skew(r2)*invI2*skew(r2)]
    //      = [1/m1+1/m2     0    ]+invI1*[r1.y*r1.y -r1.x*r1.y]+invI2*[r1.y*r1.y -r1.x*r1.y]
    //        [    0     1/m1+1/m2]       [-r1.x*r1.y r1.x*r1.x]       [-r1.x*r1.y r1.x*r1.x]
    Mat22 k1;
    k1.col1.x = body0.invMass+body1.invMass; k1.col2.x = VFloatNum!0;
    k1.col1.y = VFloatNum!0; k1.col2.y = body0.invMass+body1.invMass;

    Mat22 k2;
    k2.col1.x =  body0.invI*r1.y*r1.y; k2.col2.x = -body0.invI*r1.x*r1.y;
    k2.col1.y = -body0.invI*r1.x*r1.y; k2.col2.y =  body0.invI*r1.x*r1.x;

    Mat22 k3;
    k3.col1.x =  body1.invI*r2.y*r2.y; k3.col2.x = -body1.invI*r2.x*r2.y;
    k3.col1.y = -body1.invI*r2.x*r2.y; k3.col2.y =  body1.invI*r2.x*r2.x;

    Mat22 kmt = k1+k2+k3;
    kmt.col1.x += softness;
    kmt.col2.y += softness;

    mt = kmt.invert();

    Vec2 p1 = body0.mPosition+r1;
    Vec2 p2 = body1.mPosition+r2;
    Vec2 dp = p2-p1;

    if (World.positionCorrection) {
      bias = -biasFactor*invDt*dp;
    } else {
      bias.set(VFloatNum!0, VFloatNum!0);
    }

    if (World.warmStarting) {
      // apply accumulated impulse
      body0.velocity -= body0.invMass*accimp;
      body0.angularVelocity -= body0.invI*r1.cross(accimp);

      body1.velocity += body1.invMass*accimp;
      body1.angularVelocity += body1.invI*r2.cross(accimp);
    } else {
      accimp.set(VFloatNum!0, VFloatNum!0);
    }
  }

  void applyImpulse () {
    Vec2 dv = body1.velocity+body1.angularVelocity.fcross(r2)-body0.velocity-body0.angularVelocity.fcross(r1);
    Vec2 impulse = mt*(bias-dv-softness*accimp);

    body0.velocity -= body0.invMass*impulse;
    body0.angularVelocity -= body0.invI*r1.cross(impulse);

    body1.velocity += body1.invMass*impulse;
    body1.angularVelocity += body1.invI*r2.cross(impulse);

    accimp += impulse;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private struct Contact {
public:
  Vec2 position; // updated in collide()
  Vec2 normal; // updated in collide()
  Vec2 r1, r2;
  VFloat separation = VFloatNum!0; // updated in collide(), negative of penetration
  VFloat accimpN = VFloatNum!0; // accumulated normal impulse
  VFloat accimpT = VFloatNum!0; // accumulated tangent impulse
  VFloat accimpNb = VFloatNum!0;  // accumulated normal impulse for position bias
  VFloat massNormal, massTangent;
  VFloat bias = VFloatNum!0;
}


// ////////////////////////////////////////////////////////////////////////// //
private struct Arbiter {
public:
  enum MaxContactPoints = 2;

  static private VFloat clamp() (VFloat a, VFloat low, VFloat high) { pragma(inline, true); import std.algorithm : min, max; return max(low, min(a, high)); }

public:
  Contact[MaxContactPoints] contacts;
  int numContacts;

  BodyBase body0, body1;
  VFloat friction; // combined friction
  uint frameNum; // used to track "frame touch"

public:
  this (BodyBase b0, BodyBase b1, int aFrameNum) { setup(b0, b1, frameNum); }

  @disable this (this);

  void clear () { numContacts = 0; }

  @property bool active () { return (numContacts > 0); }

  void setup (BodyBase b0, BodyBase b1, int aFrameNum) {
    frameNum = aFrameNum;
    import core.stdc.math : sqrtf;
    if (b0 < b1) {
      body0 = b0;
      body1 = b1;
    } else {
      body0 = b1;
      body1 = b0;
    }
    numContacts = collide(contacts[], body0, body1);
    friction = sqrtf(body0.friction*body1.friction);
    if (b2dlDrawContactsCB !is null) {
      BodyContact bc;
      foreach (const ref ctx; contacts[0..numContacts]) {
        bc.position = ctx.position;
        bc.normal = ctx.normal;
        bc.separation = ctx.separation;
        b2dlDrawContactsCB(bc);
      }
    }
  }

  void preStep (VFloat invDt) {
    import std.algorithm : min;
    enum AllowedPenetration = VFloatNum!(0.01);
    immutable VFloat biasFactor = (World.positionCorrection ? VFloatNum!(0.2) : VFloatNum!0);
    foreach (immutable idx; 0..numContacts) {
      Contact *c = contacts.ptr+idx;
      Vec2 r1 = c.position-body0.mPosition;
      Vec2 r2 = c.position-body1.mPosition;

      // precompute normal mass, tangent mass, and bias
      VFloat rn1 = r1*c.normal; //Dot(r1, c.normal);
      VFloat rn2 = r2*c.normal; //Dot(r2, c.normal);
      VFloat kNormal = body0.invMass+body1.invMass;
      //kNormal += body0.invI*(Dot(r1, r1)-rn1*rn1)+body1.invI*(Dot(r2, r2)-rn2*rn2);
      kNormal += body0.invI*((r1*r1)-rn1*rn1)+body1.invI*((r2*r2)-rn2*rn2);
      c.massNormal = VFloatNum!1/kNormal;

      //Vec2 tangent = c.normal.cross(VFloatNum!1);
      Vec2 tangent = Vec2(VFloatNum!1*c.normal.y, -VFloatNum!1*c.normal.x);
      VFloat rt1 = r1*tangent; //Dot(r1, tangent);
      VFloat rt2 = r2*tangent; //Dot(r2, tangent);
      VFloat kTangent = body0.invMass+body1.invMass;
      //kTangent += body0.invI*(Dot(r1, r1)-rt1*rt1)+body1.invI*(Dot(r2, r2)-rt2*rt2);
      kTangent += body0.invI*((r1*r1)-rt1*rt1)+body1.invI*((r2*r2)-rt2*rt2);
      c.massTangent = VFloatNum!1/kTangent;

      c.bias = -biasFactor*invDt*min(VFloatNum!0, c.separation+AllowedPenetration);

      if (World.accumulateImpulses) {
        // apply normal + friction impulse
        Vec2 accimp = c.accimpN*c.normal+c.accimpT*tangent;

        body0.velocity -= body0.invMass*accimp;
        body0.angularVelocity -= body0.invI*r1.cross(accimp);

        body1.velocity += body1.invMass*accimp;
        body1.angularVelocity += body1.invI*r2.cross(accimp);
      }
    }
  }

  void applyImpulse () {
    import std.algorithm : max;
    BodyBase b0 = body0;
    BodyBase b1 = body1;
    foreach (immutable idx; 0..numContacts) {
      Contact *c = contacts.ptr+idx;
      c.r1 = c.position-b0.mPosition;
      c.r2 = c.position-b1.mPosition;

      // relative velocity at contact
      Vec2 dv = b1.velocity+b1.angularVelocity.fcross(c.r2)-b0.velocity-b0.angularVelocity.fcross(c.r1);

      // compute normal impulse
      VFloat vn = dv*c.normal; //Dot(dv, c.normal);

      VFloat dPn = c.massNormal*(-vn+c.bias);

      if (World.accumulateImpulses) {
        // clamp the accumulated impulse
        VFloat accimpN0 = c.accimpN;
        c.accimpN = max(accimpN0+dPn, VFloatNum!0);
        dPn = c.accimpN-accimpN0;
      } else {
        dPn = max(dPn, VFloatNum!0);
      }

      // apply contact impulse
      Vec2 accimpN = dPn*c.normal;

      b0.velocity -= b0.invMass*accimpN;
      b0.angularVelocity -= b0.invI*c.r1.cross(accimpN);

      b1.velocity += b1.invMass*accimpN;
      b1.angularVelocity += b1.invI*c.r2.cross(accimpN);

      // relative velocity at contact
      dv = b1.velocity+b1.angularVelocity.fcross(c.r2)-b0.velocity-b0.angularVelocity.fcross(c.r1);

      //Vec2 tangent = c.normal.cross(VFloatNum!1);
      Vec2 tangent = Vec2(VFloatNum!1*c.normal.y, -VFloatNum!1*c.normal.x);
      VFloat vt = dv*tangent; //Dot(dv, tangent);
      VFloat dPt = c.massTangent*(-vt);

      if (World.accumulateImpulses) {
        // compute friction impulse
        VFloat maxPt = friction*c.accimpN;
        // clamp friction
        VFloat oldTangentImpulse = c.accimpT;
        c.accimpT = clamp(oldTangentImpulse+dPt, -maxPt, maxPt);
        dPt = c.accimpT-oldTangentImpulse;
      } else {
        VFloat maxPt = friction*dPn;
        dPt = clamp(dPt, -maxPt, maxPt);
      }

      // apply contact impulse
      Vec2 accimpT = dPt*tangent;

      b0.velocity -= b0.invMass*accimpT;
      b0.angularVelocity -= b0.invI*c.r1.cross(accimpT);

      b1.velocity += b1.invMass*accimpT;
      b1.angularVelocity += b1.invI*c.r2.cross(accimpT);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// collide
// the normal points from A to B
// return number of contact points
// this fills:
//   position (already moved out of body)
//   normal
//   separation (this is negative penetration)
//   feature (used only in arbiter updates to merge contacts, and has no effect in b2dlite)
// return number of contacts
private int collide (Contact[] contacts, BodyBase bodyAb, BodyBase bodyBb) {
  auto pb0 = cast(PolyBody)bodyAb;
  auto pb1 = cast(PolyBody)bodyBb;
  if (pb0 is null || pb1 is null) assert(0);

  ContactInfo ci;
  polygon2Polygon(ci, pb0, pb1);
  if (!ci.valid) return 0; // no contacts

  foreach (immutable cidx; 0..ci.contactCount) {
    contacts[cidx].normal = ci.normal;
    contacts[cidx].separation = -ci.penetration;
    contacts[cidx].position = ci.contacts[cidx]+(ci.normal*ci.penetration);
  }

  return ci.contactCount;
}


// ////////////////////////////////////////////////////////////////////////// //
private struct ContactInfo {
  VFloat penetration; // depth of penetration from collision
  Vec2 normal; // from A to B
  Vec2[2] contacts; // points of contact during collision
  uint contactCount; // number of contacts that occured during collision

  @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (contactCount > 0); }
}


private bool biasGreaterThan (VFloat a, VFloat b) pure nothrow @safe @nogc {
  pragma(inline, true);
  enum k_biasRelative = VFloatNum!(0.95);
  enum k_biasAbsolute = VFloatNum!(0.01);
  return (a >= b*k_biasRelative+a*k_biasAbsolute);
}


private VFloat findAxisLeastPenetration (uint* faceIndex, PolyBody flesha, PolyBody fleshb) {
  VFloat bestDistance = -VFloat.max;
  uint bestIndex;
  foreach (immutable i; 0..flesha.verts.length) {
    // retrieve a face normal from A
    Vec2 n = flesha.norms.ptr[i];
    Vec2 nw = flesha.rmat*n;
    // transform face normal into B's model space
    Mat22 buT = fleshb.rmat.transpose();
    n = buT*nw;
    // retrieve support point from B along -n
    Vec2 s = fleshb.support(-n);
    // retrieve vertex on face from A, transform into B's model space
    Vec2 v = flesha.verts.ptr[i];
    v = flesha.rmat*v+flesha.mPosition;
    v -= fleshb.mPosition;
    v = buT*v;
    // compute penetration distance (in B's model space)
    VFloat d = n.dot(s-v);
    // store greatest distance
    if (d > bestDistance) {
      bestDistance = d;
      bestIndex = i;
    }
  }
  *faceIndex = bestIndex;
  return bestDistance;
}


private void findIncidentFace (Vec2[] v, PolyBody refPoly, PolyBody incPoly, uint referenceIndex) {
  Vec2 referenceNormal = refPoly.norms.ptr[referenceIndex];
  // calculate normal in incident's frame of reference
  referenceNormal = refPoly.rmat*referenceNormal; // to world space
  referenceNormal = incPoly.rmat.transpose()*referenceNormal; // to incident's model space
  // find most anti-normal face on incident polygon
  uint incidentFace = 0;
  VFloat minDot = VFloat.max;
  foreach (immutable i; 0..incPoly.verts.length) {
    VFloat dot = referenceNormal.dot(incPoly.norms.ptr[i]);
    if (dot < minDot) {
      minDot = dot;
      incidentFace = i;
    }
  }
  // assign face vertices for incidentFace
  v.ptr[0] = incPoly.rmat*incPoly.verts.ptr[incidentFace]+incPoly.mPosition;
  incidentFace = (incidentFace+1)%incPoly.verts.length;
  v.ptr[1] = incPoly.rmat*incPoly.verts.ptr[incidentFace]+incPoly.mPosition;
}


private uint clip (Vec2 n, VFloat c, Vec2[] face) {
  uint sp = 0;
  Vec2[2] outv = face.ptr[0..2];
  // retrieve distances from each endpoint to the line
  // d = ax + by - c
  VFloat d1 = n.dot(face.ptr[0])-c;
  VFloat d2 = n.dot(face.ptr[1])-c;
  // if negative (behind plane) clip
  if (d1 <= VFloatNum!0) outv.ptr[sp++] = face.ptr[0];
  if (d2 <= VFloatNum!0) outv.ptr[sp++] = face.ptr[1];
  // if the points are on different sides of the plane
  if (d1*d2 < VFloatNum!0) { // less than to ignore -0.0f
    // push interesection point
    VFloat alpha = d1/(d1-d2);
    if (sp >= 2) assert(0, "internal collision detection error");
    outv.ptr[sp] = face.ptr[0]+alpha*(face.ptr[1]-face.ptr[0]);
    ++sp;
  }
  // assign our new converted values
  face.ptr[0] = outv.ptr[0];
  face.ptr[1] = outv.ptr[1];
  assert(sp != 3);
  return sp;
}


private void polygon2Polygon (ref ContactInfo m, PolyBody flesha, PolyBody fleshb) {
  //flesha.rmat = Mat22(flesha.mRotation);
  //fleshb.rmat = Mat22(fleshb.mRotation);

  m.contactCount = 0;

  // check for a separating axis with A's face planes
  uint faceA;
  VFloat penetrationA = findAxisLeastPenetration(&faceA, flesha, fleshb);
  if (penetrationA >= VFloatNum!0) return;

  // check for a separating axis with B's face planes
  uint faceB;
  VFloat penetrationB = findAxisLeastPenetration(&faceB, fleshb, flesha);
  if (penetrationB >= VFloatNum!0) return;

  uint referenceIndex;
  bool flip; // Always point from a to b

  PolyBody refPoly; // Reference
  PolyBody incPoly; // Incident

  // determine which shape contains reference face
  if (biasGreaterThan(penetrationA, penetrationB)) {
    refPoly = flesha;
    incPoly = fleshb;
    referenceIndex = faceA;
    flip = false;
  } else {
    refPoly = fleshb;
    incPoly = flesha;
    referenceIndex = faceB;
    flip = true;
  }

  // world space incident face
  Vec2[2] incidentFace;
  findIncidentFace(incidentFace[], refPoly, incPoly, referenceIndex);

  //        y
  //        ^  .n       ^
  //      +---c ------posPlane--
  //  x < | i |\
  //      +---+ c-----negPlane--
  //             \       v
  //              r
  //
  //  r : reference face
  //  i : incident poly
  //  c : clipped point
  //  n : incident normal

  // setup reference face vertices
  Vec2 v1 = refPoly.verts.ptr[referenceIndex];
  referenceIndex = (referenceIndex+1)%refPoly.verts.length;
  Vec2 v2 = refPoly.verts.ptr[referenceIndex];

  // transform vertices to world space
  v1 = refPoly.rmat*v1+refPoly.mPosition;
  v2 = refPoly.rmat*v2+refPoly.mPosition;

  // calculate reference face side normal in world space
  Vec2 sidePlaneNormal = v2-v1;
  sidePlaneNormal.normalize;

  // orthogonalize
  auto refFaceNormal = Vec2(sidePlaneNormal.y, -sidePlaneNormal.x);

  // ax + by = c
  // c is distance from origin
  VFloat refC = refFaceNormal.dot(v1);
  VFloat negSide = -sidePlaneNormal.dot(v1);
  VFloat posSide =  sidePlaneNormal.dot(v2);

  // clip incident face to reference face side planes
  if (clip(-sidePlaneNormal, negSide, incidentFace) < 2) return; // due to floating point error, possible to not have required points
  if (clip(sidePlaneNormal, posSide, incidentFace) < 2) return; // due to floating point error, possible to not have required points

  // flip
  m.normal = (flip ? -refFaceNormal : refFaceNormal);

  // keep points behind reference face
  uint cp = 0; // clipped points behind reference face
  VFloat separation = refFaceNormal.dot(incidentFace[0])-refC;
  if (separation <= VFloatNum!0) {
    m.contacts[cp] = incidentFace[0];
    m.penetration = -separation;
    ++cp;
  } else {
    m.penetration = 0;
  }

  separation = refFaceNormal.dot(incidentFace[1])-refC;
  if (separation <= VFloatNum!0) {
    m.contacts[cp] = incidentFace[1];
    m.penetration += -separation;
    ++cp;
    // average penetration
    m.penetration /= cast(VFloat)cp;
  }

  m.contactCount = cp;
}


// ////////////////////////////////////////////////////////////////////////// //
/* Dynamic AABB tree (bounding volume hierarchy)
 * based on the code from ReactPhysics3D physics library, http://www.reactphysics3d.com
 * Copyright (c) 2010-2016 Daniel Chappuis
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not claim
 *    that you wrote the original software. If you use this software in a
 *    product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */
private struct AABBBase(VT) if (Vec2.isVector!VT) {
private:
  VT mMin, mMax;

public:
  alias VType = VT;

public pure nothrow @safe @nogc:
  @property ref const(VT) min () const { pragma(inline, true); return mMin; }
  @property ref const(VT) max () const { pragma(inline, true); return mMax; }

  @property void min() (in auto ref VT v) { pragma(inline, true); mMin = v; }
  @property void max() (in auto ref VT v) { pragma(inline, true); mMax = v; }

  // return the volume of the AABB
  @property VFloat volume () const {
    pragma(inline, true);
    immutable diff = mMax-mMin;
    static if (VT.isVector3!VT) {
      return diff.x*diff.y*diff.z;
    } else {
      return diff.x*diff.y;
    }
  }

  static auto mergeAABBs() (in auto ref AABB aabb1, in auto ref AABB aabb2) {
    import std.algorithm : max, min;
    typeof(this) res;
    res.merge(aabb1, aabb2);
    return res;
  }

  void merge() (in auto ref AABB aabb1, in auto ref AABB aabb2) {
    import std.algorithm : max, min;
    pragma(inline, true);
    mMin.x = min(aabb1.mMin.x, aabb2.mMin.x);
    mMin.y = min(aabb1.mMin.y, aabb2.mMin.y);
    mMax.x = max(aabb1.mMax.x, aabb2.mMax.x);
    mMax.y = max(aabb1.mMax.y, aabb2.mMax.y);
    static if (VT.isVector3!VT) {
      mMin.z = min(aabb1.mMin.z, aabb2.mMin.z);
      mMax.z = max(aabb1.mMax.z, aabb2.mMax.z);
    }
  }

  // return true if the current AABB contains the AABB given in parameter
  bool contains() (in auto ref AABB aabb) const {
    pragma(inline, true);
    bool isInside = true;
    isInside = isInside && mMin.x <= aabb.mMin.x;
    isInside = isInside && mMin.y <= aabb.mMin.y;
    isInside = isInside && mMax.x >= aabb.mMax.x;
    isInside = isInside && mMax.y >= aabb.mMax.y;
    static if (VT.isVector3!VT) {
      isInside = isInside && mMin.z <= aabb.mMin.z;
      isInside = isInside && mMax.z >= aabb.mMax.z;
    }
    return isInside;
  }

  // return true if the current AABB is overlapping with the AABB in argument
  // two AABBs overlap if they overlap in the two(three) x, y (and z) axes at the same time
  bool overlaps() (in auto ref AABB aabb) const {
    //pragma(inline, true);
    if (mMax.x < aabb.mMin.x || aabb.mMax.x < mMin.x) return false;
    if (mMax.y < aabb.mMin.y || aabb.mMax.y < mMin.y) return false;
    static if (VT.isVector3!VT) {
      if (mMax.z < aabb.mMin.z || aabb.mMax.z < mMin.z) return false;
    }
    return true;
  }
}

alias AABB = AABBBase!Vec2;


// ////////////////////////////////////////////////////////////////////////// //
private align(1) struct TreeNode {
align(1):
  enum NullTreeNode = -1;
  enum { Left = 0, Right = 1 }
  // a node is either in the tree (has a parent) or in the free nodes list (has a next node)
  union {
    int parentId;
    int nextNodeId;
  }
  // a node is either a leaf (has data) or is an internal node (has children)
  union {
    int[2] children; /// left and right child of the node (children[0] = left child)
    BodyBase flesh;
  }
  // height of the node in the tree
  short height;
  // fat axis aligned bounding box (AABB) corresponding to the node
  AABB aabb;
  // return true if the node is a leaf of the tree
  @property bool leaf () const pure nothrow @safe @nogc { pragma(inline, true); return (height == 0); }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
 * This class implements a dynamic AABB tree that is used for broad-phase
 * collision detection. This data structure is inspired by Nathanael Presson's
 * dynamic tree implementation in BulletPhysics. The following implementation is
 * based on the one from Erin Catto in Box2D as described in the book
 * "Introduction to Game Physics with Box2D" by Ian Parberry.
 */
private final class DynamicAABBTree {
  private import std.algorithm : max, min;

  // in the broad-phase collision detection (dynamic AABB tree), the AABBs are
  // also inflated in direction of the linear motion of the body by mutliplying the
  // followin constant with the linear velocity and the elapsed time between two frames
  enum VFloat LinearMotionGapMultiplier = VFloatNum!(1.7);

public:
  // called when a overlapping node has been found during the call to reportAllShapesOverlappingWithAABB()
  alias OverlapCallback = void delegate (int nodeId);

private:
  TreeNode* mNodes; // pointer to the memory location of the nodes of the tree
  int mRootNodeId; // id of the root node of the tree
  int mFreeNodeId; // id of the first node of the list of free (allocated) nodes in the tree that we can use
  int mAllocCount; // number of allocated nodes in the tree
  int mNodeCount; // number of nodes in the tree

  // extra AABB Gap used to allow the collision shape to move a little bit
  // without triggering a large modification of the tree which can be costly
  VFloat mExtraGap;

private:
  // allocate and return a node to use in the tree
  int allocateNode () {
    // if there is no more allocated node to use
    if (mFreeNodeId == TreeNode.NullTreeNode) {
      import core.stdc.stdlib : realloc;
      version(b2dlite_bvh_many_asserts) assert(mNodeCount == mAllocCount);
      // allocate more nodes in the tree
      auto newsz = mAllocCount*2;
      if (newsz-mAllocCount > 4096) newsz = mAllocCount+4096;
      TreeNode* nn = cast(TreeNode*)realloc(mNodes, newsz*TreeNode.sizeof);
      if (nn is null) assert(0, "out of memory");
      mAllocCount = newsz;
      mNodes = nn;
      // initialize the allocated nodes
      foreach (int i; mNodeCount..mAllocCount-1) {
        mNodes[i].nextNodeId = i+1;
        mNodes[i].height = -1;
      }
      mNodes[mAllocCount-1].nextNodeId = TreeNode.NullTreeNode;
      mNodes[mAllocCount-1].height = -1;
      mFreeNodeId = mNodeCount;
    }
    // get the next free node
    int freeNodeId = mFreeNodeId;
    mFreeNodeId = mNodes[freeNodeId].nextNodeId;
    mNodes[freeNodeId].parentId = TreeNode.NullTreeNode;
    mNodes[freeNodeId].height = 0;
    ++mNodeCount;
    return freeNodeId;
  }

  // release a node
  void releaseNode (int nodeId) {
    version(b2dlite_bvh_many_asserts) assert(mNodeCount > 0);
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].height >= 0);
    mNodes[nodeId].nextNodeId = mFreeNodeId;
    mNodes[nodeId].height = -1;
    mFreeNodeId = nodeId;
    --mNodeCount;
  }

  // insert a leaf node in the tree
  // the process of inserting a new leaf node in the dynamic tree is described in the book "Introduction to Game Physics with Box2D" by Ian Parberry
  void insertLeafNode (int nodeId) {
    // if the tree is empty
    if (mRootNodeId == TreeNode.NullTreeNode) {
      mRootNodeId = nodeId;
      mNodes[mRootNodeId].parentId = TreeNode.NullTreeNode;
      return;
    }

    version(b2dlite_bvh_many_asserts) assert(mRootNodeId != TreeNode.NullTreeNode);

    // find the best sibling node for the new node
    AABB newNodeAABB = mNodes[nodeId].aabb;
    int currentNodeId = mRootNodeId;
    while (!mNodes[currentNodeId].leaf) {
      int leftChild = mNodes[currentNodeId].children.ptr[TreeNode.Left];
      int rightChild = mNodes[currentNodeId].children.ptr[TreeNode.Right];

      // compute the merged AABB
      VFloat volumeAABB = mNodes[currentNodeId].aabb.volume;
      AABB mergedAABBs = AABB.mergeAABBs(mNodes[currentNodeId].aabb, newNodeAABB);
      VFloat mergedVolume = mergedAABBs.volume;

      // compute the cost of making the current node the sibbling of the new node
      VFloat costS = VFloatNum!(2.0)*mergedVolume;

      // compute the minimum cost of pushing the new node further down the tree (inheritance cost)
      VFloat costI = VFloatNum!(2.0)*(mergedVolume-volumeAABB);

      // compute the cost of descending into the left child
      VFloat costLeft;
      AABB currentAndLeftAABB = AABB.mergeAABBs(newNodeAABB, mNodes[leftChild].aabb);
      if (mNodes[leftChild].leaf) {
        costLeft = currentAndLeftAABB.volume+costI;
      } else {
        VFloat leftChildVolume = mNodes[leftChild].aabb.volume;
        costLeft = costI+currentAndLeftAABB.volume-leftChildVolume;
      }

      // compute the cost of descending into the right child
      VFloat costRight;
      AABB currentAndRightAABB = AABB.mergeAABBs(newNodeAABB, mNodes[rightChild].aabb);
      if (mNodes[rightChild].leaf) {
        costRight = currentAndRightAABB.volume+costI;
      } else {
        VFloat rightChildVolume = mNodes[rightChild].aabb.volume;
        costRight = costI+currentAndRightAABB.volume-rightChildVolume;
      }

      // if the cost of making the current node a sibbling of the new node is smaller than the cost of going down into the left or right child
      if (costS < costLeft && costS < costRight) break;

      // it is cheaper to go down into a child of the current node, choose the best child
      currentNodeId = (costLeft < costRight ? leftChild : rightChild);
    }

    int siblingNode = currentNodeId;

    // create a new parent for the new node and the sibling node
    int oldParentNode = mNodes[siblingNode].parentId;
    int newParentNode = allocateNode();
    mNodes[newParentNode].parentId = oldParentNode;
    mNodes[newParentNode].aabb.merge(mNodes[siblingNode].aabb, newNodeAABB);
    mNodes[newParentNode].height = cast(short)(mNodes[siblingNode].height+1);
    version(b2dlite_bvh_many_asserts) assert(mNodes[newParentNode].height > 0);

    // If the sibling node was not the root node
    if (oldParentNode != TreeNode.NullTreeNode) {
      version(b2dlite_bvh_many_asserts) assert(!mNodes[oldParentNode].leaf);
      if (mNodes[oldParentNode].children.ptr[TreeNode.Left] == siblingNode) {
        mNodes[oldParentNode].children.ptr[TreeNode.Left] = newParentNode;
      } else {
        mNodes[oldParentNode].children.ptr[TreeNode.Right] = newParentNode;
      }
      mNodes[newParentNode].children.ptr[TreeNode.Left] = siblingNode;
      mNodes[newParentNode].children.ptr[TreeNode.Right] = nodeId;
      mNodes[siblingNode].parentId = newParentNode;
      mNodes[nodeId].parentId = newParentNode;
    } else {
      // if the sibling node was the root node
      mNodes[newParentNode].children.ptr[TreeNode.Left] = siblingNode;
      mNodes[newParentNode].children.ptr[TreeNode.Right] = nodeId;
      mNodes[siblingNode].parentId = newParentNode;
      mNodes[nodeId].parentId = newParentNode;
      mRootNodeId = newParentNode;
    }

    // move up in the tree to change the AABBs that have changed
    currentNodeId = mNodes[nodeId].parentId;
    version(b2dlite_bvh_many_asserts) assert(!mNodes[currentNodeId].leaf);
    while (currentNodeId != TreeNode.NullTreeNode) {
      // balance the sub-tree of the current node if it is not balanced
      currentNodeId = balanceSubTreeAtNode(currentNodeId);
      version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);

      version(b2dlite_bvh_many_asserts) assert(!mNodes[currentNodeId].leaf);
      int leftChild = mNodes[currentNodeId].children.ptr[TreeNode.Left];
      int rightChild = mNodes[currentNodeId].children.ptr[TreeNode.Right];
      version(b2dlite_bvh_many_asserts) assert(leftChild != TreeNode.NullTreeNode);
      version(b2dlite_bvh_many_asserts) assert(rightChild != TreeNode.NullTreeNode);

      // recompute the height of the node in the tree
      mNodes[currentNodeId].height = cast(short)(max(mNodes[leftChild].height, mNodes[rightChild].height)+1);
      version(b2dlite_bvh_many_asserts) assert(mNodes[currentNodeId].height > 0);

      // recompute the AABB of the node
      mNodes[currentNodeId].aabb.merge(mNodes[leftChild].aabb, mNodes[rightChild].aabb);

      currentNodeId = mNodes[currentNodeId].parentId;
    }

    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);
  }

  // remove a leaf node from the tree
  void removeLeafNode (int nodeId) {
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);

    // If we are removing the root node (root node is a leaf in this case)
    if (mRootNodeId == nodeId) { mRootNodeId = TreeNode.NullTreeNode; return; }

    int parentNodeId = mNodes[nodeId].parentId;
    int grandParentNodeId = mNodes[parentNodeId].parentId;
    int siblingNodeId;

    if (mNodes[parentNodeId].children.ptr[TreeNode.Left] == nodeId) {
      siblingNodeId = mNodes[parentNodeId].children.ptr[TreeNode.Right];
    } else {
      siblingNodeId = mNodes[parentNodeId].children.ptr[TreeNode.Left];
    }

    // if the parent of the node to remove is not the root node
    if (grandParentNodeId != TreeNode.NullTreeNode) {
      // destroy the parent node
      if (mNodes[grandParentNodeId].children.ptr[TreeNode.Left] == parentNodeId) {
        mNodes[grandParentNodeId].children.ptr[TreeNode.Left] = siblingNodeId;
      } else {
        version(b2dlite_bvh_many_asserts) assert(mNodes[grandParentNodeId].children.ptr[TreeNode.Right] == parentNodeId);
        mNodes[grandParentNodeId].children.ptr[TreeNode.Right] = siblingNodeId;
      }
      mNodes[siblingNodeId].parentId = grandParentNodeId;
      releaseNode(parentNodeId);

      // now, we need to recompute the AABBs of the node on the path back to the root and make sure that the tree is still balanced
      int currentNodeId = grandParentNodeId;
      while (currentNodeId != TreeNode.NullTreeNode) {
        // balance the current sub-tree if necessary
        currentNodeId = balanceSubTreeAtNode(currentNodeId);

        version(b2dlite_bvh_many_asserts) assert(!mNodes[currentNodeId].leaf);

        // get the two children.ptr of the current node
        int leftChildId = mNodes[currentNodeId].children.ptr[TreeNode.Left];
        int rightChildId = mNodes[currentNodeId].children.ptr[TreeNode.Right];

        // recompute the AABB and the height of the current node
        mNodes[currentNodeId].aabb.merge(mNodes[leftChildId].aabb, mNodes[rightChildId].aabb);
        mNodes[currentNodeId].height = cast(short)(max(mNodes[leftChildId].height, mNodes[rightChildId].height)+1);
        version(b2dlite_bvh_many_asserts) assert(mNodes[currentNodeId].height > 0);

        currentNodeId = mNodes[currentNodeId].parentId;
      }
    } else {
      // if the parent of the node to remove is the root node, the sibling node becomes the new root node
      mRootNodeId = siblingNodeId;
      mNodes[siblingNodeId].parentId = TreeNode.NullTreeNode;
      releaseNode(parentNodeId);
    }
  }

  // balance the sub-tree of a given node using left or right rotations
  // the rotation schemes are described in the book "Introduction to Game Physics with Box2D" by Ian Parberry
  // this method returns the new root node Id
  int balanceSubTreeAtNode (int nodeId) {
    version(b2dlite_bvh_many_asserts) assert(nodeId != TreeNode.NullTreeNode);

    TreeNode* nodeA = mNodes+nodeId;

    // if the node is a leaf or the height of A's sub-tree is less than 2
    if (nodeA.leaf || nodeA.height < 2) return nodeId; // do not perform any rotation

    // get the two children nodes
    int nodeBId = nodeA.children.ptr[TreeNode.Left];
    int nodeCId = nodeA.children.ptr[TreeNode.Right];
    version(b2dlite_bvh_many_asserts) assert(nodeBId >= 0 && nodeBId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(nodeCId >= 0 && nodeCId < mAllocCount);
    TreeNode* nodeB = mNodes+nodeBId;
    TreeNode* nodeC = mNodes+nodeCId;

    // compute the factor of the left and right sub-trees
    int balanceFactor = nodeC.height-nodeB.height;

    // if the right node C is 2 higher than left node B
    if (balanceFactor > 1) {
      version(b2dlite_bvh_many_asserts) assert(!nodeC.leaf);

      int nodeFId = nodeC.children.ptr[TreeNode.Left];
      int nodeGId = nodeC.children.ptr[TreeNode.Right];
      version(b2dlite_bvh_many_asserts) assert(nodeFId >= 0 && nodeFId < mAllocCount);
      version(b2dlite_bvh_many_asserts) assert(nodeGId >= 0 && nodeGId < mAllocCount);
      TreeNode* nodeF = mNodes+nodeFId;
      TreeNode* nodeG = mNodes+nodeGId;

      nodeC.children.ptr[TreeNode.Left] = nodeId;
      nodeC.parentId = nodeA.parentId;
      nodeA.parentId = nodeCId;

      if (nodeC.parentId != TreeNode.NullTreeNode) {
        if (mNodes[nodeC.parentId].children.ptr[TreeNode.Left] == nodeId) {
          mNodes[nodeC.parentId].children.ptr[TreeNode.Left] = nodeCId;
        } else {
          version(b2dlite_bvh_many_asserts) assert(mNodes[nodeC.parentId].children.ptr[TreeNode.Right] == nodeId);
          mNodes[nodeC.parentId].children.ptr[TreeNode.Right] = nodeCId;
        }
      } else {
        mRootNodeId = nodeCId;
      }

      version(b2dlite_bvh_many_asserts) assert(!nodeC.leaf);
      version(b2dlite_bvh_many_asserts) assert(!nodeA.leaf);

      // if the right node C was higher than left node B because of the F node
      if (nodeF.height > nodeG.height) {
        nodeC.children.ptr[TreeNode.Right] = nodeFId;
        nodeA.children.ptr[TreeNode.Right] = nodeGId;
        nodeG.parentId = nodeId;

        // recompute the AABB of node A and C
        nodeA.aabb.merge(nodeB.aabb, nodeG.aabb);
        nodeC.aabb.merge(nodeA.aabb, nodeF.aabb);

        // recompute the height of node A and C
        nodeA.height = cast(short)(max(nodeB.height, nodeG.height)+1);
        nodeC.height = cast(short)(max(nodeA.height, nodeF.height)+1);
        version(b2dlite_bvh_many_asserts) assert(nodeA.height > 0);
        version(b2dlite_bvh_many_asserts) assert(nodeC.height > 0);
      } else {
        // if the right node C was higher than left node B because of node G
        nodeC.children.ptr[TreeNode.Right] = nodeGId;
        nodeA.children.ptr[TreeNode.Right] = nodeFId;
        nodeF.parentId = nodeId;

        // recompute the AABB of node A and C
        nodeA.aabb.merge(nodeB.aabb, nodeF.aabb);
        nodeC.aabb.merge(nodeA.aabb, nodeG.aabb);

        // recompute the height of node A and C
        nodeA.height = cast(short)(max(nodeB.height, nodeF.height)+1);
        nodeC.height = cast(short)(max(nodeA.height, nodeG.height)+1);
        version(b2dlite_bvh_many_asserts) assert(nodeA.height > 0);
        version(b2dlite_bvh_many_asserts) assert(nodeC.height > 0);
      }

      // return the new root of the sub-tree
      return nodeCId;
    }

    // if the left node B is 2 higher than right node C
    if (balanceFactor < -1) {
      version(b2dlite_bvh_many_asserts) assert(!nodeB.leaf);

      int nodeFId = nodeB.children.ptr[TreeNode.Left];
      int nodeGId = nodeB.children.ptr[TreeNode.Right];
      version(b2dlite_bvh_many_asserts) assert(nodeFId >= 0 && nodeFId < mAllocCount);
      version(b2dlite_bvh_many_asserts) assert(nodeGId >= 0 && nodeGId < mAllocCount);
      TreeNode* nodeF = mNodes+nodeFId;
      TreeNode* nodeG = mNodes+nodeGId;

      nodeB.children.ptr[TreeNode.Left] = nodeId;
      nodeB.parentId = nodeA.parentId;
      nodeA.parentId = nodeBId;

      if (nodeB.parentId != TreeNode.NullTreeNode) {
        if (mNodes[nodeB.parentId].children.ptr[TreeNode.Left] == nodeId) {
          mNodes[nodeB.parentId].children.ptr[TreeNode.Left] = nodeBId;
        } else {
          version(b2dlite_bvh_many_asserts) assert(mNodes[nodeB.parentId].children.ptr[TreeNode.Right] == nodeId);
          mNodes[nodeB.parentId].children.ptr[TreeNode.Right] = nodeBId;
        }
      } else {
        mRootNodeId = nodeBId;
      }

      version(b2dlite_bvh_many_asserts) assert(!nodeB.leaf);
      version(b2dlite_bvh_many_asserts) assert(!nodeA.leaf);

      // if the left node B was higher than right node C because of the F node
      if (nodeF.height > nodeG.height) {
        nodeB.children.ptr[TreeNode.Right] = nodeFId;
        nodeA.children.ptr[TreeNode.Left] = nodeGId;
        nodeG.parentId = nodeId;

        // recompute the AABB of node A and B
        nodeA.aabb.merge(nodeC.aabb, nodeG.aabb);
        nodeB.aabb.merge(nodeA.aabb, nodeF.aabb);

        // recompute the height of node A and B
        nodeA.height = cast(short)(max(nodeC.height, nodeG.height)+1);
        nodeB.height = cast(short)(max(nodeA.height, nodeF.height)+1);
        version(b2dlite_bvh_many_asserts) assert(nodeA.height > 0);
        version(b2dlite_bvh_many_asserts) assert(nodeB.height > 0);
      } else {
        // if the left node B was higher than right node C because of node G
        nodeB.children.ptr[TreeNode.Right] = nodeGId;
        nodeA.children.ptr[TreeNode.Left] = nodeFId;
        nodeF.parentId = nodeId;

        // recompute the AABB of node A and B
        nodeA.aabb.merge(nodeC.aabb, nodeF.aabb);
        nodeB.aabb.merge(nodeA.aabb, nodeG.aabb);

        // recompute the height of node A and B
        nodeA.height = cast(short)(max(nodeC.height, nodeF.height)+1);
        nodeB.height = cast(short)(max(nodeA.height, nodeG.height)+1);
        version(b2dlite_bvh_many_asserts) assert(nodeA.height > 0);
        version(b2dlite_bvh_many_asserts) assert(nodeB.height > 0);
      }

      // return the new root of the sub-tree
      return nodeBId;
    }

    // if the sub-tree is balanced, return the current root node
    return nodeId;
  }

  // compute the height of a given node in the tree
  int computeHeight (int nodeId) {
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    TreeNode* node = mNodes+nodeId;

    // If the node is a leaf, its height is zero
    if (node.leaf) return 0;

    // Compute the height of the left and right sub-tree
    int leftHeight = computeHeight(node.children.ptr[TreeNode.Left]);
    int rightHeight = computeHeight(node.children.ptr[TreeNode.Right]);

    // Return the height of the node
    return 1+max(leftHeight, rightHeight);
  }

  // internally add an object into the tree
  int addObjectInternal() (in auto ref AABB aabb) {
    // get the next available node (or allocate new ones if necessary)
    int nodeId = allocateNode();

    // create the fat aabb to use in the tree
    immutable gap = AABB.VType(mExtraGap, mExtraGap, mExtraGap);
    mNodes[nodeId].aabb.min = aabb.min-gap;
    mNodes[nodeId].aabb.max = aabb.max+gap;

    // set the height of the node in the tree
    mNodes[nodeId].height = 0;

    // insert the new leaf node in the tree
    insertLeafNode(nodeId);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);

    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0);

    // return the Id of the node
    return nodeId;
  }

  // initialize the tree
  void setup () {
    import core.stdc.stdlib : malloc;
    import core.stdc.string : memset;

    mRootNodeId = TreeNode.NullTreeNode;
    mNodeCount = 0;
    mAllocCount = 64;

    mNodes = cast(TreeNode*)malloc(mAllocCount*TreeNode.sizeof);
    if (mNodes is null) assert(0, "out of memory");
    memset(mNodes, 0, mAllocCount*TreeNode.sizeof);

    // initialize the allocated nodes
    foreach (int i; 0..mAllocCount-1) {
      mNodes[i].nextNodeId = i+1;
      mNodes[i].height = -1;
    }
    mNodes[mAllocCount-1].nextNodeId = TreeNode.NullTreeNode;
    mNodes[mAllocCount-1].height = -1;
    mFreeNodeId = 0;
  }

  // also, checks if the tree structure is valid (for debugging purpose)
  void forEachLeaf (scope void delegate (int nodeId, in ref AABB aabb) dg) {
    void forEachNode (int nodeId) {
      if (nodeId == TreeNode.NullTreeNode) return;
      // if it is the root
      if (nodeId == mRootNodeId) {
        assert(mNodes[nodeId].parentId == TreeNode.NullTreeNode);
      }
      // get the children nodes
      TreeNode* pNode = mNodes+nodeId;
      assert(pNode.height >= 0);
      assert(pNode.aabb.volume > 0);
      // if the current node is a leaf
      if (pNode.leaf) {
        assert(pNode.height == 0);
        if (dg !is null) dg(nodeId, pNode.aabb);
      } else {
        int leftChild = pNode.children.ptr[TreeNode.Left];
        int rightChild = pNode.children.ptr[TreeNode.Right];
        // check that the children node Ids are valid
        assert(0 <= leftChild && leftChild < mAllocCount);
        assert(0 <= rightChild && rightChild < mAllocCount);
        // check that the children nodes have the correct parent node
        assert(mNodes[leftChild].parentId == nodeId);
        assert(mNodes[rightChild].parentId == nodeId);
        // check the height of node
        int height = 1+max(mNodes[leftChild].height, mNodes[rightChild].height);
        assert(mNodes[nodeId].height == height);
        // check the AABB of the node
        AABB aabb = AABB.mergeAABBs(mNodes[leftChild].aabb, mNodes[rightChild].aabb);
        assert(aabb.min == mNodes[nodeId].aabb.min);
        assert(aabb.max == mNodes[nodeId].aabb.max);
        // recursively check the children nodes
        forEachNode(leftChild);
        forEachNode(rightChild);
      }
    }
    // recursively check each node
    forEachNode(mRootNodeId);
  }

public:
  this (VFloat extraAABBGap=VFloatNum!0) {
    mExtraGap = extraAABBGap;
    setup();
  }

  ~this () {
    import core.stdc.stdlib : free;
    free(mNodes);
  }

  // return the fat AABB corresponding to a given node Id
  /*const ref*/ AABB getFatAABB (int nodeId) const {
    pragma(inline, true);
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    return mNodes[nodeId].aabb;
  }

  // return the pointer to the data array of a given leaf node of the tree
  BodyBase getNodeBody (int nodeId) {
    pragma(inline, true);
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);
    return mNodes[nodeId].flesh;
  }

  // return the root AABB of the tree
  AABB getRootAABB () { pragma(inline, true); return getFatAABB(mRootNodeId); }

  // add an object into the tree.
  // this method creates a new leaf node in the tree and returns the Id of the corresponding node
  int addObject (BodyBase flesh) {
    auto aabb = flesh.getAABB(); // can be passed as argument
    int nodeId = addObjectInternal(aabb);
    mNodes[nodeId].flesh = flesh;
    return nodeId;
  }

  // remove an object from the tree
  void removeObject (int nodeId) {
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);
    // remove the node from the tree
    removeLeafNode(nodeId);
    releaseNode(nodeId);
  }

  // update the dynamic tree after an object has moved
  // if the new AABB of the object that has moved is still inside its fat AABB, then nothing is done.
  // otherwise, the corresponding node is removed and reinserted into the tree.
  // the method returns true if the object has been reinserted into the tree.
  // the "displacement" argument is the linear velocity of the AABB multiplied by the elapsed time between two frames.
  // if the "forceReinsert" parameter is true, we force a removal and reinsertion of the node
  // (this can be useful if the shape AABB has become much smaller than the previous one for instance).
  // return `true` if the tree was modified
  bool updateObject() (int nodeId, in auto ref AABB.VType displacement, bool forceReinsert=false) {
    version(b2dlite_bvh_many_asserts) assert(nodeId >= 0 && nodeId < mAllocCount);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].leaf);
    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].height >= 0);

    auto newAABB = mNodes[nodeId].flesh.getAABB(); // can be passed as argument

    // if the new AABB is still inside the fat AABB of the node
    if (!forceReinsert && mNodes[nodeId].aabb.contains(newAABB)) return false;

    // if the new AABB is outside the fat AABB, we remove the corresponding node
    removeLeafNode(nodeId);

    // compute the fat AABB by inflating the AABB with a constant gap
    mNodes[nodeId].aabb = newAABB;
    immutable gap = AABB.VType(mExtraGap, mExtraGap, mExtraGap);
    mNodes[nodeId].aabb.mMin -= gap;
    mNodes[nodeId].aabb.mMax += gap;

    // inflate the fat AABB in direction of the linear motion of the AABB
    if (displacement.x < VFloatNum!0) {
      mNodes[nodeId].aabb.mMin.x += LinearMotionGapMultiplier*displacement.x;
    } else {
      mNodes[nodeId].aabb.mMax.x += LinearMotionGapMultiplier*displacement.x;
    }
    if (displacement.y < VFloatNum!0) {
      mNodes[nodeId].aabb.mMin.y += LinearMotionGapMultiplier*displacement.y;
    } else {
      mNodes[nodeId].aabb.mMax.y += LinearMotionGapMultiplier*displacement.y;
    }
    static if (AABB.VType.isVector3!(AABB.VType)) {
      if (displacement.z < VFloatNum!0) {
        mNodes[nodeId].aabb.mMin.z += LinearMotionGapMultiplier *displacement.z;
      } else {
        mNodes[nodeId].aabb.mMax.z += LinearMotionGapMultiplier *displacement.z;
      }
    }

    version(b2dlite_bvh_many_asserts) assert(mNodes[nodeId].aabb.contains(newAABB));

    // reinsert the node into the tree
    insertLeafNode(nodeId);

    return true;
  }

  // report all shapes overlapping with the AABB given in parameter
  void reportAllShapesOverlappingWithAABB() (in auto ref AABB aabb, scope OverlapCallback callback) {
    int[256] stack = void; // stack with the nodes to visit
    int sp = 0;

    void spush (int id) {
      if (sp >= stack.length) throw new Exception("stack overflow");
      stack.ptr[sp++] = id;
    }

    int spop () {
      if (sp == 0) throw new Exception("stack underflow");
      return stack.ptr[--sp];
    }

    spush(mRootNodeId);

    // while there are still nodes to visit
    while (sp > 0) {
      // get the next node id to visit
      int nodeIdToVisit = spop();
      // skip it if it is a null node
      if (nodeIdToVisit == TreeNode.NullTreeNode) continue;
      // get the corresponding node
      const(TreeNode)* nodeToVisit = mNodes+nodeIdToVisit;
      // if the AABB in parameter overlaps with the AABB of the node to visit
      if (aabb.overlaps(nodeToVisit.aabb)) {
        // if the node is a leaf
        if (nodeToVisit.leaf) {
          // notify the broad-phase about a new potential overlapping pair
          callback(nodeIdToVisit);
        } else {
          // if the node is not a leaf
          // we need to visit its children
          spush(nodeToVisit.children.ptr[TreeNode.Left]);
          spush(nodeToVisit.children.ptr[TreeNode.Right]);
        }
      }
    }
  }

  // compute the height of the tree
  int computeHeight () { pragma(inline, true); return computeHeight(mRootNodeId); }

  // clear all the nodes and reset the tree
  void reset() {
    import core.stdc.stdlib : free;
    free(mNodes);
    setup();
  }
}
